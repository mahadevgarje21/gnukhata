
package gnukhata.views;


import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.reportmodels.grossTrialBalance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;

public class viewgrosstrialbalreport extends Composite{
	
	Color Background;
	Color Foreground;
	Color FocusBackground;
	Color FocusForeground;
	Color BtnFocusForeground;
	Color lightBlue;
	Color tabalternate; 
    Color tabalternate1;
    
    int tblfocusindex;
    
	static Display display;
	ODPackage sheetStream;
	int counter = 0;
	TableViewer tblgrosstrialbal;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn totaldr;
	TableColumn totalcr;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lbltotaldr;
	Label lbltotalcr;
	Button btnViewTbForAccount;
	Button btnPrint;
    String strdate;
    Color bgbtnColor;
    Color fgbtnColor;
    Color fgtblColor;
    Color bgtblColor;
    Color bgtxtColor;
    Color fgtxtColor;
   
	NumberFormat nf;
	Vector<Object> printGrossTrial = new Vector<Object>();
	ArrayList<Button> accounts = new ArrayList<Button>();
	int shellwidth = 0;
	
	
	String endDateParam = "";
	public viewgrosstrialbalreport(Composite parent, String endDate,int style, ArrayList<grossTrialBalance> grossdata)
	{
		super(parent,style);
		endDateParam = endDate;
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		//Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		//layout.top = new FormAttachment(1);
		//layout.left = new FormAttachment(63);
		//layout.right = new FormAttachment(87);
		//layout.bottom = new FormAttachment(9);
		
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		//lblLogo.setLocation(getClientArea().width, getClientArea().height);
		//lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		//lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1].toString().replace("&", "&&"));
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(53);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails.setLayoutData(layout);

		Label lblOrgDetails1 = new Label(this,SWT.NONE);
		lblOrgDetails1.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails1.setText("For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(0);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(3);
		lblOrgDetails1.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(5);
		lblLine.setLayoutData(layout);
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 12, SWT.ITALIC | SWT.BOLD) );
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		lblOrgDetails.setText("Gross Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
		layout = new FormData();
		layout.top = new FormAttachment(5);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(8);
		lblOrgDetails.setLayoutData(layout);
		
		tblgrosstrialbal = new TableViewer(this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		tblgrosstrialbal.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		tblgrosstrialbal.getTable().setLinesVisible (true);
		tblgrosstrialbal.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblOrgDetails,10);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(92);
		tblgrosstrialbal.getTable().setLayoutData(layout);
		tblgrosstrialbal.getControl().forceFocus();
		//tblgrosstrialbal.getControl().setBackground(Display.getDefault().getSystemColor(SWT.COLOR_DARK_MAGENTA));
		//tblgrosstrialbal.getControl().setForeground(Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
		
		
		btnViewTbForAccount =new Button(this,SWT.PUSH);
		btnViewTbForAccount.setText("&Back To Trial Balance");
		btnViewTbForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblgrosstrialbal.getTable(),15);
		layout.left=new FormAttachment(35);
		btnViewTbForAccount.setLayoutData(layout);

		
	    btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblgrosstrialbal.getTable(),15);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);

		//this.makeaccessible(tblgrosstrialbal);
		this.getAccessible();
		
		//this.setEvents();
		//this.pack();
		
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		
		
		shellwidth = this.getClientArea().width;
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/GrossTrialBal.ots"),"GrossTrialBal");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		tabalternate =  new Color(this.getDisplay(),255, 255, 214);
		tabalternate1 =  new Color(this.getDisplay(),184, 255, 148);
		
		Background =  new Color(this.getDisplay() ,220 , 224, 227);
		Foreground = new Color(this.getDisplay() ,0, 0,0 );
		FocusBackground  = new Color(this.getDisplay(),78,97,114 );
		FocusForeground = new Color(this.getDisplay(),255,255,255);
		BtnFocusForeground=new Color(this.getDisplay(), 0, 0, 255);
		lightBlue = new Color(this.getDisplay(),215,242,251);
		globals.setThemeColor(this, Background, Foreground);
		tblgrosstrialbal.getControl().setBackground(lightBlue);
		globals.SetButtonColoredFocusEvents(this, FocusBackground, BtnFocusForeground, Background, Foreground);
		globals.SetComboColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);
		//globals.SetTableColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground); 
		globals.SetTextColoredFocusEvents(this, FocusBackground, FocusForeground, Background, Foreground);

		this.setReport(grossdata);
		this.setEvents(grossdata);

		
	/*	bgbtnColor=this.btnViewTbForAccount.getBackground();
		fgbtnColor=this.btnViewTbForAccount.getForeground();
		bgtblColor=this.btnViewTbForAccount.getBackground();
		fgtblColor=this.btnViewTbForAccount.getForeground();
		bgtxtColor=this.btnViewTbForAccount.getBackground();
		fgtxtColor=this.btnViewTbForAccount.getForeground();
	*/	
		
	}
	
	private void setReport(ArrayList<grossTrialBalance> grossdata)
	{
		
		tblgrosstrialbal.getControl().forceFocus();
		
		TableViewerColumn colSrNo = new TableViewerColumn(tblgrosstrialbal, SWT.None);
		colSrNo.getColumn().setText("Sr.No.");
		colSrNo.getColumn().setAlignment(SWT.LEFT);
		colSrNo.getColumn().setWidth(4 * shellwidth /100);
		colSrNo.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.grossTrialBalance gross = (gnukhata.controllers.reportmodels.grossTrialBalance) element;
			return gross.getSrNo();
			}
		});
		TableViewerColumn colaccname = new TableViewerColumn(tblgrosstrialbal,SWT.None);
		colaccname.getColumn().setText("                                     Account Name");
		colaccname.getColumn().setWidth(36 * shellwidth /100);
		colaccname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.grossTrialBalance accname = (gnukhata.controllers.reportmodels.grossTrialBalance) element;
				return accname.getAccountName();
				//return super.getText(element);
			}
		}
		);
		
		
		
		TableViewerColumn coltotaldr = new TableViewerColumn(tblgrosstrialbal, SWT.None);
		coltotaldr.getColumn().setText("Debit                      ");
		coltotaldr.getColumn().setAlignment(SWT.RIGHT);
		coltotaldr.getColumn().setWidth(18 * shellwidth /100);
		coltotaldr.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.grossTrialBalance dr = (gnukhata.controllers.reportmodels.grossTrialBalance) element;
				try {
					Double drtotal = Double.parseDouble(dr.getTotaldr());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(drtotal);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					return "";
					
				}

				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn coltotalcr = new TableViewerColumn(tblgrosstrialbal, SWT.None);
		coltotalcr.getColumn().setText("Credit                       ");
		coltotalcr.getColumn().setAlignment(SWT.RIGHT);
		coltotalcr.getColumn().setWidth(16 * shellwidth /100);
		coltotalcr.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.grossTrialBalance cr = (gnukhata.controllers.reportmodels.grossTrialBalance) element;
				try {
					Double crtotal = Double.parseDouble(cr.getTotalcr());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(crtotal);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block

					e.printStackTrace();
					return "";
				}

				//return super.getText(element);
			}
		}
		);

		TableViewerColumn colgrpname = new TableViewerColumn(tblgrosstrialbal, SWT.None);
		colgrpname.getColumn().setText("                 Group Name");
		colgrpname.getColumn().setAlignment(SWT.LEFT);
		colgrpname.getColumn().setWidth(19 * shellwidth /100);
		colgrpname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.grossTrialBalance grpname = (gnukhata.controllers.reportmodels.grossTrialBalance) element;
				return grpname.getGroupName();
				//return super.getText(element);
			}
		}
		);
		
		tblgrosstrialbal.setContentProvider(new ArrayContentProvider());
		tblgrosstrialbal.setInput(grossdata);
		
		TableItem[] items1 = tblgrosstrialbal.getTable().getItems();
		for (int rowid=0; rowid<items1.length; rowid++){
		    if (rowid%2==0) 
		    {
		    	items1[rowid].setBackground(tabalternate);
		    }
		    else {
		    	items1[rowid].setBackground(tabalternate1);
		    }
		}
		
		tblgrosstrialbal.getTable().setFocus();
		tblgrosstrialbal.getTable().setSelection(0);
		//tblgrosstrialbal.getTable().pack();
		
	}
	private void setEvents(final ArrayList<grossTrialBalance> grossdata)
	{
		tblgrosstrialbal.getTable().addFocusListener(new FocusAdapter() {
			
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				tblfocusindex = tblgrosstrialbal.getTable().getSelectionIndex();
				tblgrosstrialbal.getTable().setSelection(-1);
			}
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				tblgrosstrialbal.getTable().setSelection(tblfocusindex);
			}
		});
		
		
		
		
		//now the tableviewr needs to be set the key event and mouse event.
				tblgrosstrialbal.getControl().addMouseListener(new MouseAdapter() {
					@Override
					public void mouseDoubleClick(MouseEvent arg0) {
						// TODO Auto-generated method stub
						IStructuredSelection selection = (IStructuredSelection) tblgrosstrialbal.getSelection();
						grossTrialBalance gtb = (grossTrialBalance) selection.getFirstElement();
						if(gtb.getAccountName().equals("Total") || gtb.getAccountName().equals("Difference in Trial Balance"))
						{
							return;
						}
						String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
						Composite grandParent = (Composite) tblgrosstrialbal.getTable().getParent().getParent();
						String accName = gtb.getAccountName();
						reportController.showLedger(grandParent, accName,fromdate,endDateParam, "No Project", true, true, false,"Gross Trial Balance","" );
						tblgrosstrialbal.getTable().getParent().dispose();

						
						//super.mouseDoubleClick(arg0);
					}
				});
				
				/*tblgrosstrialbal.getControl().addFocusListener(new FocusAdapter() {
					
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						tblgrosstrialbal.getControl().setBackground(bgtblColor);
						tblgrosstrialbal.getControl().setForeground(fgtblColor);
					}
				});*/
				
				btnViewTbForAccount.addFocusListener(new FocusAdapter() {
					@Override
					public void focusGained(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusGained(arg0);
						btnViewTbForAccount.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
						btnViewTbForAccount.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
					}
					@Override
					public void focusLost(FocusEvent arg0) {
						// TODO Auto-generated method stub
						//super.focusLost(arg0);
						btnViewTbForAccount.setBackground(bgbtnColor);
						btnViewTbForAccount.setForeground(fgbtnColor);
					}
				});
				
				tblgrosstrialbal.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
					@Override
					public void keyPressed(KeyEvent arg0) {
						// TODO Auto-generated method stub
						if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
						{
							//drilldown here, make a call to showLedger.
							IStructuredSelection selection = (IStructuredSelection) tblgrosstrialbal.getSelection();
							grossTrialBalance gtb = (grossTrialBalance) selection.getFirstElement();
							try {
								if(gtb.getAccountName().equals("Total") || gtb.getAccountName().equals("Difference in Trial Balance"))
									{
										return;
									}
									String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
									Composite grandParent = (Composite) tblgrosstrialbal.getTable().getParent().getParent();
									String accName = gtb.getAccountName();
									reportController.showLedger(grandParent, accName,fromdate,endDateParam, "No Project", true, true, false,"Gross Trial Balance","" );
									tblgrosstrialbal.getTable().getParent().dispose();
							} catch (NullPointerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							}
					}

					});
				
		/*btnViewTbForAccount.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
			}
		});*/
		
				btnViewTbForAccount.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent arg0) {
						// TODO Auto-generated method stub
						//super.widgetSelected(arg0);
						
						
						Composite grandParent = (Composite) btnViewTbForAccount.getParent().getParent();
						btnViewTbForAccount.getParent().dispose();
							
							viewTrialBalance vl=new viewTrialBalance(grandParent,SWT.NONE);
							vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
						}
				
				});
				
		btnPrint.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewTbForAccount.setFocus();
				}
			}
		});
		
		btnPrint.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusGained(arg0);
				btnPrint.setBackground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));
				btnPrint.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
			}
			@Override
			public void focusLost(FocusEvent arg0) {
				// TODO Auto-generated method stub
				//super.focusLost(arg0);
				btnPrint.setBackground(bgbtnColor);
				btnPrint.setForeground(fgbtnColor);
			}
		});
			
				btnPrint.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				btnViewTbForAccount.setFocus();
				//TableModel model = new DefaultTableModel(finaldata,strPrintCol);
				try 
				{
					final File GrossTrialBalReport = new File("/tmp/gnukhata/Report_Output/GrossTrialBal" );
					final Sheet GrossTrialBalReportSheet = sheetStream.getSpreadSheet().getFirstSheet();
					GrossTrialBalReportSheet.ensureRowCount(100000);
					GrossTrialBalReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
					GrossTrialBalReportSheet.getCellAt(0, 1).setValue("Gross Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
					for(int rowcounter = 0; rowcounter < grossdata.size(); rowcounter ++ )
					{
						GrossTrialBalReportSheet.getCellAt(0,rowcounter +3).setValue(grossdata.get(rowcounter).getSrNo());
						GrossTrialBalReportSheet.getCellAt(1,rowcounter +3).setValue(grossdata.get(rowcounter).getAccountName());
						GrossTrialBalReportSheet.getCellAt(2,rowcounter +3).setValue(grossdata.get(rowcounter).getTotaldr());
						GrossTrialBalReportSheet.getCellAt(3,rowcounter +3).setValue(grossdata.get(rowcounter).getTotalcr());
						GrossTrialBalReportSheet.getCellAt(4,rowcounter +3).setValue(grossdata.get(rowcounter).getGroupName());
					}
					OOUtils.open(GrossTrialBalReportSheet.getSpreadSheet().saveAs(GrossTrialBalReport));
					//OOUtils.open(AccountReport);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});	
		}
	
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s= new Shell(d);
		/*int vouchercode = 0;
		String voucherType = null;
		viewTrialBalReport vtbr=new viewTrialBalReport(s, SWT.NONE);
		vtbr.setSize(s.getClientArea().width, s.getClientArea().height );
		
		//s.setSize(400, 400);
		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
		}
		
	}*/


}
